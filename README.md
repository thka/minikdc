# miniKDC

With this script a kerberos KDC is not needed.
This script will create keytab and a credential cache and now you can login to your destination server without providing a password.

WARNING!

When the keytab is installed on your destination server, this script makes it possible to login to your destination server with ANY user account.
This means your computer must be as secure as a KDC in a production environment.

## Requirements

Pytab library to read and write keytab files and credential cache files

git clone https://gitlab.com/thka/pytab

## Getting started

```
./minikdc.py --keytab krb5.keytab --ccache /tmp/krb5cc_1000 --realm INTERNAL.LAN --username user --fqdn server.internal.lan
```

### Check credentials

```
klist

Ticket cache: FILE:/tmp/krb5cc_1000
Default principal: user@INTERNAL.LAN

Valid starting       Expires              Service principal
2024-03-31 13:23:50  2024-03-31 23:23:49  krbtgt/INTERNAL.LAN@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  host/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  vpn/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  ldap/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  cifs/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  nfs/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  HTTP/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  https/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  IMAP/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  IMAP4/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  pop/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  xmpp/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  ftp/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  ipp/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  SMTP/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
2024-03-31 13:23:50  2024-03-31 23:23:49  vnc/server.internal.lan@INTERNAL.LAN
	renew until 2024-03-31 23:23:49
```

## Setup services

### SSH

Copy krb5.keytab to you destination server:/etc/krb5.keytab

Edit /etc/ssh/sshd_config and set "GSSAPIAuthentication yes"

systemctl restart ssh


### Imap/Pop

Copy krb5.keytab to you destination server:/etc/krb5.keytab

apt install dovecot-pop3d dovecot-imapdovecot-gssapi

Edit /etc/dovecot/conf.d/10-auth.conf

```
auth_realms = INTERNAL.LAN
auth_default_realm = INTERNAL.LAN
auth_username_format = %n
auth_mechanisms = gssapi
```

### Imap/Pop

Copy krb5.keytab to you destination server:/etc/krb5.keytab

apt install apache2 libapache2-mod-auth-gssapi
a2enmod ssl

<Directory /var/www/html>
    AuthType                GSSAPI
    AuthName                "miniKDC"
    GssapiCredStore         keytab:/etc/krb5.keytab
    GssapiAcceptorName      HTTP
    GssapiNegotiateOnce     On
    GssapiSSLonly           On
    GssapiLocalName         On
    # Use a cookie to keep the session, avoid reauthenticate user on each page
    # (facultative)
    GssapiUseSessions       On
    GssapiDelegCcacheDir    /run/apache2/clientcaches
    <IfModule mod_session.c>
        Session on
    </IfModule>
    <IfModule mod_session_cookie.c>
        SessionCookieName gssapi_session path=/;httponly;secure;
    </IfModule>

    Require valid-user
</Directory>

