#!/usr/bin/env python3

# MiniKDC
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# e-post thomas.karlsson@relea.se

import os
import sys
import argparse
import secrets
from impacket.krb5.crypto import Key, _enctype_table
from pyasn1.codec.der import encoder, decoder
from pyasn1.type import univ, namedtype, char, useful, tag, constraint
from pyasn1.type.tag import Tag, tagClassContext, tagFormatSimple, tagClassApplication
from binascii import unhexlify
from pytab.pytab.pytab import (Keytab, Credential_cache, CC_Principal, Header_entry,
                               Credential, KrbFlags, Keyentry, Principal)
import datetime
from typing import List

# MAX = float('inf')


class Int32(univ.Integer):
    pass


Int32.subtypeSpec = constraint.ValueRangeConstraint(-2147483648, 2147483647)


class UInt32(univ.Integer):
    pass


UInt32.subtypeSpec = constraint.ValueRangeConstraint(0, 4294967295)


class KerberosString(char.GeneralString):
    pass


class Realm(KerberosString):
    pass


class PrincipalName(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('name-type', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('name-string', univ.SequenceOf(componentType=KerberosString()).subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))
    )


class EncryptionKey(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('keytype', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('keyvalue', univ.OctetString().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))
    )


class TransitedEncoding(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('tr-type', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('contents', univ.OctetString().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))
    )


class KerberosTime(useful.GeneralizedTime):
    pass


class HostAddress(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('addr-type', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('address', univ.OctetString().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))
    )


class HostAddresses(univ.SequenceOf):
    componentType = HostAddress()


class AuthorizationData(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('ad-type', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('ad-data', univ.OctetString().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))
    )


class KerberosFlags(univ.BitString):
    pass


# KerberosFlags.subtypeSpec=constraint.ValueSizeConstraint(32, MAX)
KerberosFlags.subtypeSpec = constraint.ValueSizeConstraint(32, 64)


class TicketFlags(KerberosFlags):
    pass


class EncTicketPart(univ.Sequence):
    tagSet = univ.Sequence.tagSet.tagExplicitly(tag.Tag(tag.tagClassApplication, tag.tagFormatConstructed, 3))
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('flags', TicketFlags().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0))),
        namedtype.NamedType('key', EncryptionKey().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1))),
        namedtype.NamedType('crealm', Realm().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2))),
        namedtype.NamedType('cname', PrincipalName().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 3))),
        namedtype.NamedType('transited', TransitedEncoding().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 4))),
        namedtype.NamedType('authtime', KerberosTime().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 5))),
        namedtype.OptionalNamedType('starttime', KerberosTime().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 6))),
        namedtype.NamedType('endtime', KerberosTime().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 7))),
        namedtype.OptionalNamedType('renewtill', KerberosTime().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 8))),
        namedtype.OptionalNamedType('caddr', univ.SequenceOf(componentType=HostAddress()).subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 9))),
        namedtype.OptionalNamedType('authorization-data', univ.SequenceOf(componentType=AuthorizationData()).subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 10)))
    )


class EncryptedData(univ.Sequence):
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('etype', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.OptionalNamedType('kvno', UInt32(tagSet=UInt32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)))),
        namedtype.NamedType('cipher', univ.OctetString().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2)))
        # namedtype.NamedType('cipher', EncTicketPart().subtype(implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2)))
    )


class Ticket(univ.Sequence):
    tagSet = univ.Sequence.tagSet.tagExplicitly(
        tag.Tag(tagClassApplication, tagFormatSimple, 1)
    )
    componentType = namedtype.NamedTypes(
        namedtype.NamedType('tktvno', Int32(tagSet=Int32.tagSet.tagExplicitly(tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)))),
        namedtype.NamedType('realm', Realm().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1))),
        namedtype.NamedType('sname', PrincipalName().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2))),
        namedtype.NamedType('enc-part', EncryptedData().subtype(explicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 3)))
    )


def create_cc_header() -> Header_entry:
    header = Header_entry()

    header.tag = 1  # Time
    header.tagdata = b'\x00\x00\x00\x00\x00\x00\x00\x00'

    return header


def create_login_principal(name_type: int, realm: str, username: str) -> CC_Principal:
    login_principal = CC_Principal()

    login_principal.name_type = name_type
    login_principal.realm = realm
    login_principal.components.append(username)

    return login_principal


def create_server_service_cache(service: str, arguments: argparse.Namespace, keytabkey: bytes, kvno: int) -> Credential:
    realm = arguments.realm
    spn_service = service.split('/')[0]
    spn_host = service.split('/')[1]
    username = arguments.username
    encryption_algorithm = int(arguments.encryption_type)

    # Credential
    server_credential = Credential()
    # client_principal
    server_credential.client_principal = create_login_principal(1, realm, username)

    # server_principal
    server_principal = CC_Principal()
    server_principal.name_type = 3
    server_principal.realm = realm
    server_principal.components.append(spn_service)
    server_principal.components.append(spn_host)
    server_credential.server_principal = server_principal

    # key_type to AES 18(0x12)
    server_credential.key_type = encryption_algorithm

    # encryption_type
    server_credential.encryption_type = 0

    # encryption_key
    # server_credential.encryption_key = unhexlify('167013454344df6a1e7aa7f23c30f81b3581901c1b711c3d3774e311d7a1b049')
    session_key = arguments.sessionkey
    # AES256 key is 32 bytes
    if arguments.sessionkey is None:
        if arguments.verbose:
            print('[INFO] Using random key for session key')
        session_key = secrets.token_hex(32)
    else:
        if len(arguments.sessionkey) != 32:
            if arguments.verbose:
                print('[INFO] Using specified session key')
            session_key = secrets.token_hex(32)

    server_credential.encryption_key = unhexlify(session_key)

    # times
    auth_time = datetime.datetime.now()
    # auth_time = datetime.datetime.fromtimestamp(1710599864)
    utc_delta = datetime.timedelta(hours=1)
    auth_time_utc = auth_time - utc_delta

    start_time_delta = datetime.timedelta(seconds=1)
    end_time_delta = datetime.timedelta(seconds=arguments.validfor)
    renew_till_delta = datetime.timedelta(seconds=arguments.validfor)

    start_time = auth_time + start_time_delta
    # start_time = datetime.datetime.fromtimestamp(1710599934)
    start_time_utc = start_time - utc_delta

    end_time = auth_time + end_time_delta
    # end_time = datetime.datetime.fromtimestamp(1710635864)
    end_time_utc = end_time - utc_delta

    renew_till = auth_time + renew_till_delta
    # renew_till = datetime.datetime.fromtimestamp(1710686263)
    renew_till_utc = renew_till - utc_delta

    server_credential.auth_time = int(auth_time.timestamp())
    server_credential.start_time = int(start_time.timestamp())
    server_credential.end_time = int(end_time.timestamp())
    server_credential.renew_till = int(renew_till.timestamp())

    # skey
    server_credential.skey = 0

    # ticket_flags
    # server_credential.ticket_flags = 1353252864
    server_credential.ticket_flags |= KrbFlags.FORWARDABLE.value
    server_credential.ticket_flags |= KrbFlags.PROXIABLE.value
    server_credential.ticket_flags |= KrbFlags.RENEWABLE.value
    server_credential.ticket_flags |= KrbFlags.PRE_AUTHENTICATION.value
    server_credential.ticket_flags |= KrbFlags.TRANSITED_POLICY_CHECKED.value
    server_credential.ticket_flags |= KrbFlags.ANONYMOUS.value

    # addresses
    server_credential.addresses = []

    # authdata
    server_credential.authdata = []

    # ticket
    server_ticket = Ticket()
    server_ticket['tktvno'] = 5
    server_ticket['realm'] = realm
    server_ticket['sname']['name-type'] = 3
    server_ticket['sname']['name-string'].append(spn_service)
    server_ticket['sname']['name-string'].append(spn_host)

    # EncryptedData
    server_ticket['enc-part']['etype'] = encryption_algorithm

    encticketpart = EncTicketPart()
    # encticketpart['flags'] = 0
    # encticketpart['flags'] = KrbFlags.FORWARDABLE.value | KrbFlags.PROXIABLE.value | KrbFlags.RENEWABLE.value | KrbFlags.PRE_AUTHENTICATION.value | KrbFlags.TRANSITED_POLICY_CHECKED.value
    # encticketpart['flags'] = b'\x00\x01\x00\x01\x00\x00\x00\x00\x01\x00\x01\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    encticketpart['flags'] = '01010000101010010000000000000000'

    # encticketpart['flags'] |= KrbFlags.FORWARDABLE.value
    # encticketpart['flags'] |= KrbFlags.PROXIABLE.value
    # encticketpart['flags'] |= KrbFlags.RENEWABLE.value
    # encticketpart['flags'] |= KrbFlags.PRE_AUTHENTICATION.value
    # encticketpart['flags'] |= KrbFlags.TRANSITED_POLICY_CHECKED.value
    # encticketpart['flags'] = 1353252864

    encticketpart['key']['keytype'] = encryption_algorithm
    # kvno 2 encticketpart['key']['keyvalue'] = unhexlify('5e983e6c34edcb71d061af18cdce020b051df005a94bf7d62c16cddbeff3952d')
    # kvno 3
    # encticketpart['key']['keyvalue'] = unhexlify('167013454344df6a1e7aa7f23c30f81b3581901c1b711c3d3774e311d7a1b049')
    encticketpart['key']['keyvalue'] = unhexlify(session_key)

    encticketpart['crealm']._value = realm
    encticketpart['cname']['name-type'] = 1
    encticketpart['cname']['name-string'].append(username)

    encticketpart['transited']['tr-type'] = 1
    encticketpart['transited']['contents'] = b''

    authtime_string = f'{auth_time_utc.year}{auth_time_utc.month:02d}{auth_time_utc.day:02d}{auth_time_utc.hour:02d}{auth_time_utc.minute:02d}{auth_time_utc.second:02d}Z'
    starttime_string = f'{start_time_utc.year}{start_time_utc.month:02d}{start_time_utc.day:02d}{start_time_utc.hour:02d}{start_time_utc.minute:02d}{start_time_utc.second:02d}Z'
    endtime_string = f'{end_time_utc.year}{end_time_utc.month:02d}{end_time_utc.day:02d}{end_time_utc.hour:02d}{end_time_utc.minute:02d}{end_time_utc.second:02d}Z'
    renewtill_string = f'{renew_till_utc.year}{renew_till_utc.month:02d}{renew_till_utc.day:02d}{renew_till_utc.hour:02d}{renew_till_utc.minute:02d}{renew_till_utc.second:02d}Z'
    encticketpart['authtime'] = authtime_string
    encticketpart['starttime'] = starttime_string
    encticketpart['endtime'] = endtime_string
    encticketpart['renewtill'] = renewtill_string

    addata = AuthorizationData()
    addata['ad-type'] = 1
    addata['ad-data'] = unhexlify('302c302aa00402020200a1220420301ea003020112a1173015a003020110a10e040c78df0a5a45f2cdf09f24719d')
    encticketpart['authorization-data'].append(addata)

    encrypt_key = Key(encryption_algorithm, keytabkey)
    cipher = _enctype_table[encryption_algorithm]
    # encryptedText = cipher.encrypt(encrypt_key, 2, encoder.encode(encticketpart), confounder=unhexlify(b'f80141bd626a96929e60781de50e279a'))
    encryptedText = cipher.encrypt(encrypt_key, 2, encoder.encode(encticketpart), confounder=None)
    server_ticket['enc-part']['kvno'] = kvno
    server_ticket['enc-part']['cipher'] = encryptedText

    server_credential.ticket = encoder.encode(server_ticket)

    return server_credential


def create_krbtgt_cache(arguments: argparse.Namespace) -> Credential:
    realm = arguments.realm
    username = arguments.username
    encryption_algorithm = int(arguments.encryption_type)

    # Credential
    krbtgt_credential = Credential()
    # client_principal
    krbtgt_credential.client_principal = create_login_principal(1, realm, username)

    # server_principal
    krbtgt_principal = CC_Principal()
    krbtgt_principal.name_type = 2
    krbtgt_principal.realm = realm
    krbtgt_principal.components.append('krbtgt')
    krbtgt_principal.components.append(realm)
    krbtgt_credential.server_principal = krbtgt_principal

    # key_type to AES 18(0x12)
    krbtgt_credential.key_type = encryption_algorithm

    # encryption_type
    krbtgt_credential.encryption_type = 0

    # encryption_key
    # Not needed, there is no communication over the network to a real KDC
    krbtgt_credential.encryption_key = unhexlify('1111111111111111111111111111111111111111111111111111111111111111')

    # times
    auth_time = datetime.datetime.now()
    # auth_time = datetime.datetime.fromtimestamp(1710599864)
    utc_delta = datetime.timedelta(hours=1)
    auth_time_utc = auth_time - utc_delta

    start_time_delta = datetime.timedelta(seconds=1)
    end_time_delta = datetime.timedelta(seconds=arguments.validfor)
    renew_till_delta = datetime.timedelta(seconds=arguments.validfor)

    start_time = auth_time + start_time_delta
    # start_time = datetime.datetime.fromtimestamp(1710599934)
    start_time_utc = start_time - utc_delta

    end_time = auth_time + end_time_delta
    # end_time = datetime.datetime.fromtimestamp(1710635864)
    end_time_utc = end_time - utc_delta

    renew_till = auth_time + renew_till_delta
    # renew_till = datetime.datetime.fromtimestamp(1710686263)
    renew_till_utc = renew_till - utc_delta

    krbtgt_credential.auth_time = int(auth_time.timestamp())
    krbtgt_credential.start_time = int(start_time.timestamp())
    krbtgt_credential.end_time = int(end_time.timestamp())
    krbtgt_credential.renew_till = int(renew_till.timestamp())

    # skey
    krbtgt_credential.skey = 0

    # ticket_flags
    # server_credential.ticket_flags = 1356922880
    krbtgt_credential.ticket_flags |= KrbFlags.FORWARDABLE.value
    krbtgt_credential.ticket_flags |= KrbFlags.PROXIABLE.value
    krbtgt_credential.ticket_flags |= KrbFlags.RENEWABLE.value
    krbtgt_credential.ticket_flags |= KrbFlags.PRE_AUTHENTICATION.value
    krbtgt_credential.ticket_flags |= KrbFlags.INITIAL.value
    krbtgt_credential.ticket_flags |= KrbFlags.ANONYMOUS.value

    # addresses
    krbtgt_credential.addresses = []

    # authdata
    krbtgt_credential.authdata = []

    # ticket
    krbtgt_ticket = Ticket()
    krbtgt_ticket['tktvno'] = 5
    krbtgt_ticket['realm'] = realm
    krbtgt_ticket['sname']['name-type'] = 2
    krbtgt_ticket['sname']['name-string'].append('krbtgt')
    krbtgt_ticket['sname']['name-string'].append(realm)

    # EncryptedData
    krbtgt_ticket['enc-part']['etype'] = arguments.encryption_type

    encticketpart = EncTicketPart()
    # encticketpart['flags'] = 0
    # encticketpart['flags'] = KrbFlags.FORWARDABLE.value | KrbFlags.PROXIABLE.value | KrbFlags.RENEWABLE.value | KrbFlags.PRE_AUTHENTICATION.value | KrbFlags.TRANSITED_POLICY_CHECKED.value
    # encticketpart['flags'] = b'\x00\x01\x00\x01\x00\x00\x00\x00\x01\x00\x01\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    encticketpart['flags'] = '01010000101010010000000000000000'

    # encticketpart['flags'] |= KrbFlags.FORWARDABLE.value
    # encticketpart['flags'] |= KrbFlags.PROXIABLE.value
    # encticketpart['flags'] |= KrbFlags.RENEWABLE.value
    # encticketpart['flags'] |= KrbFlags.PRE_AUTHENTICATION.value
    # encticketpart['flags'] |= KrbFlags.TRANSITED_POLICY_CHECKED.value
    # encticketpart['flags'] = 1353252864

    encticketpart['key']['keytype'] = encryption_algorithm
    encticketpart['key']['keyvalue'] = unhexlify('1111111111111111111111111111111111111111111111111111111111111111')
    encticketpart['crealm'] = realm
    encticketpart['cname']['name-type'] = 1
    encticketpart['cname']['name-string'].append(username)

    encticketpart['transited']['tr-type'] = 1
    encticketpart['transited']['contents'] = b''

    authtime_string = f'{auth_time_utc.year}{auth_time_utc.month:02d}{auth_time_utc.day:02d}{auth_time_utc.hour:02d}{auth_time_utc.minute:02d}{auth_time_utc.second:02d}Z'
    starttime_string = f'{start_time_utc.year}{start_time_utc.month:02d}{start_time_utc.day:02d}{start_time_utc.hour:02d}{start_time_utc.minute:02d}{start_time_utc.second:02d}Z'
    endtime_string = f'{end_time_utc.year}{end_time_utc.month:02d}{end_time_utc.day:02d}{end_time_utc.hour:02d}{end_time_utc.minute:02d}{end_time_utc.second:02d}Z'
    renewtill_string = f'{renew_till_utc.year}{renew_till_utc.month:02d}{renew_till_utc.day:02d}{renew_till_utc.hour:02d}{renew_till_utc.minute:02d}{renew_till_utc.second:02d}Z'
    encticketpart['authtime'] = authtime_string
    encticketpart['starttime'] = starttime_string
    encticketpart['endtime'] = endtime_string
    encticketpart['renewtill'] = renewtill_string

    addata = AuthorizationData()
    addata['ad-type'] = 1
    addata['ad-data'] = unhexlify('302c302aa00402020200a1220420301ea003020112a1173015a003020110a10e040cabd2c2d93d1d18ea8875ac4c')
    encticketpart['authorization-data'].append(addata)

    encrypt_key = Key(arguments.encryption_type, unhexlify('2222222222222222222222222222222222222222222222222222222222222222'))
    cipher = _enctype_table[arguments.encryption_type]
    # encryptedText = cipher.encrypt(encrypt_key, 2, encoder.encode(encticketpart), confounder=unhexlify(b'298942df5e9fb38b8a41cd22d9258481'))
    encryptedText = cipher.encrypt(encrypt_key, 2, encoder.encode(encticketpart), confounder=None)
    krbtgt_ticket['enc-part']['kvno'] = 1
    krbtgt_ticket['enc-part']['cipher'] = encryptedText

    krbtgt_credential.ticket = encoder.encode(krbtgt_ticket)

    return krbtgt_credential


def create_configuration_one(arguments: argparse.Namespace) -> Credential:
    client_entry = CC_Principal()
    client_entry.realm = arguments.realm
    client_entry.name_type = 1
    client_entry.components.append('thka')

    server_entry = CC_Principal()
    server_entry.realm = 'X-CACHECONF:'
    server_entry.name_type = 0
    server_entry.components.append('krb5_ccache_conf_data')
    server_entry.components.append('fast_avail')
    # server_entry.components.append('refresh_time')
    server_entry.components.append(f'krbtgt/{arguments.realm}@{arguments.realm}')

    config_entry = Credential()
    config_entry.configuration_entry = True
    config_entry.client_principal = client_entry
    config_entry.server_principal = server_entry
    config_entry.ticket = b'yes'
    # config_entry.ticket = b'[epoch_timestamp]'

    return config_entry


def create_configuration_two(arguments: argparse.Namespace) -> Credential:
    client_entry = CC_Principal()
    client_entry.realm = arguments.realm
    client_entry.name_type = 1
    client_entry.components.append('thka')

    server_entry = CC_Principal()
    server_entry.realm = 'X-CACHECONF:'
    server_entry.name_type = 0
    server_entry.components.append('krb5_ccache_conf_data')
    server_entry.components.append('pa_type')
    server_entry.components.append(f'krbtgt/{arguments.realm}@{arguments.realm}')

    config_entry = Credential()
    config_entry.configuration_entry = True
    config_entry.client_principal = client_entry
    config_entry.server_principal = server_entry
    config_entry.ticket = b'2'

    return config_entry


def create_keytab(services: List[str], arguments: argparse.Namespace) -> None:
    new_keytab = Keytab()
    key_long = secrets.token_bytes(32)
    key_short = secrets.token_bytes(16)
    for one_spn in services:
        new_principal = Principal()
        new_principal.realm = arguments.realm
        new_principal.name_type = 1
        for comp in one_spn.split('/'):
            new_principal.components.append(comp)

        new_keyentry = Keyentry()
        new_keyentry.principal = new_principal
        timetravel = datetime.timedelta(hours=24)

        new_keyentry.timestamp = int((datetime.datetime.now() - timetravel).timestamp())
        new_keyentry.kvno = 42
        new_keyentry.kvno_extended = 42
        if arguments.encryption_type == 18:
            new_keyentry.key = key_long
        elif arguments.encryption_type == 17:
            new_keyentry.key = key_short
        new_keyentry.encryption_type = arguments.encryption_type

        new_keytab.key_entries.append(new_keyentry)

    new_keytab.save(arguments.keytab)

    return None


def get_default_services(servername: str) -> List[str]:
    svcs = ['host', 'vpn', 'ldap', 'cifs', 'nfs', 'HTTP', 'https', 'IMAP',
            'IMAP4', 'pop', 'xmpp', 'ftp', 'ipp', 'SMTP', 'vnc']

    return [f'{one}/{servername}' for one in svcs]


def main():
    parser = argparse.ArgumentParser(description='Mini KDC',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog="""
Example:\n

Create default SPN's
  minikdc.py --keytab krb5.keytab --ccache /tmp/krb5cc_1000 --realm INTERNAL.LAN --username user --fqdn server.internal.lan
Create specific SPN
  minikdc.py --keytab krb5.keytab --ccache /tmp/krb5cc_1000 --realm INTERNAL.LAN --username user --spn host/server.internal.lan
"""
                                     )
    parser.add_argument('-k',
                        '--keytab',
                        dest='keytab',
                        action='store',
                        help='The servers keytab file',
                        required=True)
    parser.add_argument('-c',
                        '--ccache',
                        dest='ccache',
                        action='store',
                        help='The credential cache file',
                        required=True)
    parser.add_argument('-e',
                        '--encryption',
                        dest='encryption_type',
                        action='store',
                        type=int,
                        choices=[17, 18],
                        help='The encryption type to use, 18(aes256), aes128 etc., default is 18',
                        default=18,
                        required=False)
    parser.add_argument('-r',
                        '--realm',
                        dest='realm',
                        action='store',
                        help='Realm',
                        required=True)
    parser.add_argument('-u',
                        '--username',
                        dest='username',
                        action='store',
                        help='Username to login with',
                        required=True)
    parser.add_argument('--validfor',
                        dest='validfor',
                        action='store',
                        type=int,
                        help='Ticket is valid for X seconds, default 36000 seconds(10 hours)',
                        default=36000,
                        required=False)
    parser.add_argument('--sessionkey',
                        dest='sessionkey',
                        action='store',
                        type=str,
                        help='Session key to use, 32 bytes hex (ab43ff2e...) defauls is random',
                        default=None,
                        required=False)
    parser.add_argument('-v',
                        '--verbose',
                        dest='verbose',
                        action='store_true',
                        help='Verbose output',
                        default=False,
                        required=False)
    parser.add_argument('-i',
                        '--instructions',
                        dest='instructions',
                        action='store_true',
                        help='Show instructions how to transfer keytab to server',
                        default=False,
                        required=False)
    mutual = parser.add_mutually_exclusive_group(required=True)
    mutual.add_argument('-f', '--fqdn',
                        dest='fqdn',
                        action='store',
                        help='FQDN for the SPN',
                        required=False)
    mutual.add_argument('-s',
                        '--spn',
                        dest='spn',
                        action='store',
                        nargs='+',
                        help='Server SPN to use (host/server.internal.lan)',
                        required=False)

    args = parser.parse_args()

    if args.spn is not None:
        services = args.spn
    else:
        services = get_default_services(args.fqdn)
        # services.append('cifs/server')
        # services.append('cifs/server$')

    if not os.path.exists(args.keytab):
        _ = create_keytab(services, args)

    server_keytab = Keytab()
    server_keytab.load(args.keytab)
    server_keyentry = Keyentry()

    for oneentry in server_keytab.entries():
        if oneentry.encryption_type == args.encryption_type:
            server_keyentry = oneentry

    new_ccache = Credential_cache()
    new_ccache.headers.append(create_cc_header())

    new_ccache.primary_principal = create_login_principal(1, args.realm, args.username)

    krbtgt_credential = create_krbtgt_cache(args)
    config_one = create_configuration_one(args)
    config_two = create_configuration_two(args)

    new_ccache.credential_entries.append(krbtgt_credential)
    new_ccache.credential_entries.append(config_one)
    new_ccache.credential_entries.append(config_two)

    for one_spn in services:
        new_ccache.credential_entries.append(create_server_service_cache(one_spn, args, server_keyentry.key, server_keyentry.kvno))

    new_ccache.save(args.ccache)

    if args.instructions:
        print('Instructions:')
        print()
        print(f'Copy {args.keytab} to destination_server:/etc/krb5.keytab')
        print('Make sure the krb5.keytab has correct permissions')
        print()


if __name__ == '__main__':
    main()
